function Jx = Jx(x,k_konc)
sum=0;
for i=1:k_konc+1
    sum=sum + x(1,i)^2 +x(2,i)^2 +x(3,i)^2;
end
Jx=sum/k_konc;

