function [x,u] = konc(k_konc,K)
x = zeros(3,k_konc);
u = zeros(k_konc);
x(1:3,1)=[-3; -1; 1];
for i=2:k_konc+1
   x(1,i)=403.3*x(1,i-1)+(-4.367)*x(2,i-1)+0.0111*x(3,i-1)+u(i-1);
   x(2,i)=x(1,i-1);
   x(3,i)=x(2,i-1);
   u=-K*x;
end

