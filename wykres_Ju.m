function wykres_Ju
 b = [17.85 -16.55 0.1337];
 a = [1 -403.3 4.367 -0.01111];
 [A,B]=tf2ss(b,a);
 k_konc=16;
 sum=0;
 z=0:0.1:1;
 length = size(z);
 J=zeros(0,length(2));
 for i=1:length(2)
     p=[0.1,0.1,z(i)];
     %p=[z(i),z(i),z(i)];
     K = acker(A,B,p);
     [x,u] = konc(k_konc,K);
    for k=1:k_konc
        sum=sum + u(i)^2;
    end
    J(i)=sum/k_konc;
    sum=0;
 end
 %figure
 semilogy(z,J);
 xlim([0 1])
 ylim([0 10^11])