function wykres_Jx
 b = [17.85 -16.55 0.1337];
 a = [1 -403.3 4.367 -0.01111];
 [A,B,C,D]=tf2ss(b,a);
 %p=[0.3 0.3 0.3];%bieguny
 %K = acker(A,B,p);
 k_konc=16;
 sum=0;
 z=0:0.1:1;
 length = size(z);
 J=zeros(0,length(2));
 for i=1:length(2)
     p=[0.1,0.1,z(i)];
     K = acker(A,B,p);
     [x,u] = konc(k_konc,K);
    for k=1:k_konc
        sum=sum + x(1,k)^2 +x(2,k)^2 +x(3,k)^2;
    end
    J(i)=sum/k_konc;
    sum=0;
 end
 %figure
 semilogy(z,J);
 ylim([0 10^11])